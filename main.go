package main

import (
	"log"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

func main() {
	r := gin.Default()

	r.Static("/download", "./file")
	r.POST("/upload", func(c *gin.Context) {
		// single file
		file, err := c.FormFile("file")
		if err != nil {
			log.Fatal(err)
		}
		log.Println(file.Filename)

		fileId := uuid.Must(uuid.NewV4())

		fileName := fileId.String() + "_" + file.Filename
		filePath := "file/" + fileName

		err = c.SaveUploadedFile(file, filePath)
		if err != nil {
			log.Fatal(err)
		}

		c.JSON(200, gin.H{
			"fileId": fileName,
		})
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
